const PosArgs = Array{Array}
mutable struct TPrmSpace
	args::PosArgs
	kwargs::KwArgs
end
Base.:(==)(a::TPrmSpace, b::TPrmSpace) = ((a.args == b.args) && (a.kwargs == b.kwargs))
TPrmSpace(a) = TPrmSpace(a, Dict())
TPrmSpace() = TPrmSpace([],Dict())

struct MultiArgs{T}
	v::Vector{T}
end
struct MultiFuns{T}
	t::Tuple
end

macro multi(code)
	Base.remove_linenums!(code)
	escape_symbols!(code)
	# return :(MultiArgs($code))
	return code
end

macro multi_funs(code)
	Base.remove_linenums!(code)
	escape_symbols!(code)
	return :(MultiFuns($code))
end

ismultiargs(e) = e isa Expr && (e.head == :call && (e.args[1] == :MultiArgs || (e.args[1] isa GlobalRef && e.args[1].name == :MultiArgs)))
vectify(e) = ismultiargs(e) ? e.args[2] : :([$e])
function escape_symbols!(ex, sh = 1)
	Base.remove_linenums!(ex) # REM?
	for i = sh:length(ex.args)
		a = ex.args[i]
		if a isa Symbol #|| ex.args[i] isa GlobalRef
			ex.args[i] = esc(ex.args[i])
		elseif a isa Expr 
			escape_symbols!(ex.args[i], (a.head in [:macrocall, :kw] ? 2 : 1))
		end
	end
	return ex
end
function replace_symbols!(ex, ar)
	Base.remove_linenums!(ex) # REM?
	for i = 1:length(ex.args)
		a = ex.args[i]
		if a isa Symbol || ex.args[i] isa GlobalRef
			s = (a isa Symbol ? a : a.name)
			if s in ar
				ex.args[i] = :(WorkflowResult($(QuoteNode(ex.args[i]))))
			end
		elseif a isa Expr 
			replace_symbols!(ex.args[i], ar)
		end
	end
	return ex
end

"""
Convert one function call of the workflow to a pair
"""
function convert_call(e::Expr, assigned_symbols = [])
@debug "entering convert_call"
	Base.remove_linenums!(e)
	replace_symbols!(e, assigned_symbols)	# Add WorkflowResult where required

	@assert e.head == :call
	args = e.args[2:end]
	kws = filter(x -> (x isa Expr && x.head == :kw), args)
	prms = filter(x -> (x isa Expr && x.head == :parameters), args)
	kwargs = isempty(prms) ? [] : prms[1].args
	for kw in kws push!(kwargs, kw) end
	kwargs_pairs = [:($(QuoteNode(kw.args[1])) => $(vectify(kw.args[2])) ) for kw in kwargs]
	remaining = filter(x -> ~(x isa Expr) || (x isa Expr && x.head != :kw && x.head != :parameters), args)
	remaining_sp = [vectify(e) for e in remaining]
	exp_args = Expr(:vect, remaining_sp...)
	exp_kwargs = Expr(:call, :KwArgs, kwargs_pairs...)
	# r = :($(esc(e.args[1])) => TPrmSpace($exp_args, $exp_kwargs))
	r = :($(e.args[1]) => TPrmSpace($exp_args, $exp_kwargs))
	escape_symbols!(r)
	@debug "end of convert_call, got " r
	return r
end

symbol(s::Symbol) = s
symbol(e::GlobalRef) = e.name
ismultifuns(e) = e.head == :call && (e.args[1] == :MultiFuns || (e.args[1] isa GlobalRef && e.args[1].name == :MultiFuns))

function convert_assign(e::Expr, assigned_symbols = [])
	Base.remove_linenums!(e)
	@assert e.head == :(=)
	rhs = e.args[2]
	s = symbol(e.args[1])
	res = if ismultifuns(rhs)
		vec_rhs = rhs.args[2].args
		:(($(QuoteNode(s)), [$([convert_call(el, assigned_symbols) for el in vec_rhs]...)] ))
	else
		:(($(QuoteNode(s)), [$(convert_call(e.args[2], assigned_symbols))] ))
	end
	push!(assigned_symbols, s)
	return res
end

macro macro_convert_call(e)
	Base.remove_linenums!(e)
	return convert_call(e)
end

macro workflow(e, ex)
@info "entry" ex
	Base.remove_linenums!(ex)
@info "without linenums" ex
# @info "\nCalling workflow on $ex"
	ex = macroexpand(Main, ex)	# for @multi and @multi_funs
@info "after expanding @multi/@multi_funs" ex
	assigned_symbols = Symbol[]
	for el in ex.args
		println("=================")
		@info "convert_assign on " el
		exp = convert_assign(el, assigned_symbols)
		@info "produces " exp
		@info "produces " eval(exp)
		# @info "i.e. " :($exp)
		println("------------------------")
	end
	flist = [convert_assign(el, assigned_symbols) for el in ex.args]
	r = :($(esc(e)).workflow = [$(flist...)])
	# dump(r,maxdepth=30)
@info "final workflow is " r
	return r
end

export PosArgs, TPrmSpace, MultiFuns, MultiArgs, replace_symbols!
export @multi, @multi_funs, @workflow, convert_call

macro mtest(e)
	Base.remove_linenums!(e)
	e = macroexpand(Main, e)
	escape_symbols!(e)
	# res = convert_call(e)
	res = e
	dump(res, maxdepth=30)
	return res
end
export @mtest


module ExperimentsManager

using DataFrames
using CSV
using Requires
using Distributed
using Base.Threads
using DocStringExtensions
using LinearAlgebra
using Logging, LoggingExtras
using Dates # For adding dates to Logging
using ProgressMeter # For parallel loop advancement

export Args, KwArgs
export PrmSpace, PrmSet, FunCall
export WorkflowResult, WorkflowPrm
export Exp, UnitExp, ExpPrms, run, prun
export Variable, addvar, logdict
export vcat_addmissing
export runlocally
export resolve
export local_results, get_results
export isrunninglocally
export expand_kwargs

include("prmspaces.jl")
include("experiments.jl")
include("backends.jl")
include("utils.jl")

include("syntaxic_sugar.jl")

function __init__()
	@require Igrida="135fe8fa-043a-11e9-0b4d-694a09e407b7" begin
		using .Igrida
		include("igrida_backend.jl")
	end
end

end # module

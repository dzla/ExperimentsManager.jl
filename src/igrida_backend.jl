export igrida, igresults, updatedev

struct IgridaExpBackend <: AbstractExpBackend
	c::IgridaConnection
	function IgridaExpBackend()
		b = new(IgridaConnection(modules = ["ExperimentsManager"]))
		return b
	end
end

function igrida(force = false, kwargs...)
	global CURRENT_EXPBACKEND
	if !(typeof(CURRENT_EXPBACKEND) <: IgridaExpBackend) || force
		finalize(CURRENT_EXPBACKEND)
		CURRENT_EXPBACKEND = IgridaExpBackend(kwargs...)
	end
	return
end

Base.finalize(b::IgridaExpBackend) = finalize(b.c)

_update_backend(b::IgridaExpBackend, e) = Igrida.updatefrontend(b.c, e.prms.modules, e.prms.datadeps)
_resultsbasedir(b::IgridaExpBackend) = adapt_resultsdir(b.c, "results_IgridaExpBE")

function _run_on_backend(b::IgridaExpBackend, fun, fun_args::Array{UnitExp}, fun_kwargs; 
				  slowdown = false, kwargs...)

	ue = fun_args[1]
	jobname = "$(ue.prms.name)__$(ue.basename)"[1:min(end,98)]	# Just for security
	logdictpath = joinpath(ue.prms.resultsdir, "$(ue.prms.logdict.name)")
	lines_computed = Igrida.nbcomputedlines(b.c, logdictpath, ue.basename)
	# println("Already computed $lines_computed out of $(ue.prms.iterations) (path=$logdictpath, bn=$(ue.basename)).")
	rem_iterations = ue.prms.iterations - lines_computed
	if isrunning(b.c, jobname) println("$jobname is already running, ignoring") end
	if rem_iterations == 0 println("Results already present for $jobname, ignoring.") end
	if ~(isrunning(b.c, jobname) || rem_iterations == 0)
		exp = quote
			using ExperimentsManager
			$fun($fun_args...; 
				 remaining_iterations = $rem_iterations, 
				 $fun_kwargs...)
		end
		# println("Launching job with name =$(length(jobname)) and length $(length(string(exp))).")
		strmods = string.(ue.prms.modules)
		fdsmask = ("FatDatasets" .== strmods)
		# fix to load FatDatasets first (problem with ImageMagick and Zlib otherwise)
		ue.prms.modules = cat(ue.prms.modules[.~fdsmask], ue.prms.modules[fdsmask], dims=1)
		for m in ue.prms.modules
			pushfirst!(exp.args, :(using $(Symbol(m))))	# fix from 1.1
			# pushfirst!(exp.args, :(using $m))	# before 1.1
		end
		println("Running $jobname (missing $rem_iterations).")
		slowdown && sleep(0.01)
		# println(exp)
		submitjob(b.c, exp; jobname = jobname, kwargs...)
	end
	return nothing
end

function _get_results(b::IgridaExpBackend)
	expdir = "results_IgridaExpBE"	# cf. _resultsbasedir
	fetch_results(b.c, expdir)
end
function _get_results(b::IgridaExpBackend, s)
	expdir = "results_IgridaExpBE"	# cf. _resultsbasedir
	fetch_results(b.c, joinpath(expdir, "results_$s"), "results_IgridaExpBE")
end

igresults(s) = local_results(s; resdir = "results_IgridaExpBE")

resolve() = Igrida.resolve_frontend(CURRENT_EXPBACKEND.c)

updatedev(n) = addmodules(CURRENT_EXPBACKEND.c, [n])

isrunningigrida() = CURRENT_EXPBACKEND isa IgridaExpBackend

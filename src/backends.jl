#######################################################################
#                         Experiment backends                         #
#######################################################################

abstract type AbstractExpBackend end
mutable struct LocalExpBackend <: AbstractExpBackend end
CURRENT_EXPBACKEND = LocalExpBackend()
expbackend() = CURRENT_EXPBACKEND

run_on_backend(fun, fun_args=[], fun_kwargs=Dict(); backend_kwargs...) = 
	_run_on_backend(CURRENT_EXPBACKEND, fun, fun_args, fun_kwargs; backend_kwargs...)
update_backend(e) = _update_backend(CURRENT_EXPBACKEND, e)
get_results(args...) = _get_results(CURRENT_EXPBACKEND, args...)
resultsbasedir() = _resultsbasedir(CURRENT_EXPBACKEND)
logs_dir(ue) = _logs_dir(CURRENT_EXPBACKEND, ue)

# Defaults
function Base.finalize(x::AbstractExpBackend) end
function _update_backend(x::AbstractExpBackend, args...) end

# ===== Local Backend =====

	# lines_computed = nbcomputedlines(b.c, logdictpath, ue.basename)
function nbcomputedlines(path, basename)
	r = joinpath(path, basename)
	# Sum numbers of lines of all files path/basename* (-1 line of header for each file)
	# Return 0 if there are no such files
	strcmd = `sh -c "if ls $r* 1> /dev/null 2>&1; then sed -s 1d $r* | wc -l; else echo 0; fi"`
	@debug "Running command '$strcmd'"
	r = read(strcmd, String)
	@debug "std out/err" stdout stderr
	try
		return parse(Int, r)
	catch e
		@error "Error trying to parse to Int the string '$r' obtained by running the command '$strcmd'."
		rethrow(e)
	end
end

_logs_dir(::LocalExpBackend, ue) = joinpath(ue.prms.resultsdir, "logs")

function _run_on_backend(::LocalExpBackend, fun, fun_args, fun_kwargs; 
					continue_on_errors = true,  
					log_to_files = true, 
					log_to_console = false, 
					logging_level = Logging.Info, 
					kwargs...)
	if ~isempty(kwargs)
		@warn "Ignoring following kwargs: $(join([kw[1] for kw in kwargs], ", ")) (not supported by LocalExpBackend)."
	end
	ue = fun_args[1]
	jobname = "$(ue.prms.name)__$(ue.basename)"[1:min(end,98)]	# Just for security
	logdictpath = joinpath(ue.prms.resultsdir, "$(ue.prms.logdict.name)")
	log_path = logs_dir(ue)
	@debug "Path to results" logdictpath
	lines_computed = nbcomputedlines(logdictpath, ue.basename)
	rem_iterations = ue.prms.iterations - lines_computed
	@debug "Remaining iterations: $rem_iterations" 
	if rem_iterations == 0
		@info "Results already present for $jobname, ignoring."
	elseif rem_iterations > 0
		old_stdout = stdout
		old_stderr = stderr
		if log_to_files 
			filename = joinpath(log_path, "$(ue.basename)-worker$(myid()).txt")
			@info "Running $jobname (remaining iterations: $(rem_iterations)/$(ue.prms.iterations)$(log_to_files ? ", redirecting logs to '$filename'" : "") and 'continue_on_errors' is $(continue_on_errors))…"
			io = open(filename, "w+")
			logger = FileLogger(io, always_flush=true)
			logger_wid = logger_with_wid(logger; min_level=logging_level)
			final_logger = if log_to_console
				TeeLogger(current_logger(), logger_wid)
			else
				logger_wid
			end
			old_logger = global_logger(final_logger)
			@info "This is the log file for job '$(jobname)', runned on worker $(myid())."
			@info "Experiment workflow:" ue.workflow
			@info "Experiments parameter are:" ue.prms
			redirect_stdout(io)
			redirect_stderr(io)
		end
		got_error = false
		try
			fun(fun_args...; remaining_iterations = rem_iterations, fun_kwargs...)
		catch e
			got_error = true
			if continue_on_errors
				@error "Something went wrong. Exception catched by ExperimentsManager for logging. Below are the error and stacktrace."
				@error e
				bt = stacktrace(catch_backtrace())
				bt_with_newlines = repr("text/plain", bt)
				@error bt
			else
				rethrow(e)
			end
		finally
			if log_to_files
				@debug "Closing log file and resetting stdout/stderr/logger to their old values."
				close(io)
				global_logger(old_logger)
				redirect_stdout(old_stdout)
				redirect_stderr(old_stderr)
			end
		end
		@info "The function call from _run_on_backend returned $(got_error ? "WITH ERRORS" : "without error")."
	else
		@warn("More observations than expected. This should not happen.")
	end
end
# _get_results(::LocalExpBackend, s) = error("Not implemented yet.") # TODO
_resultsbasedir(::LocalExpBackend) = joinpath(pwd(), "results_LocalExpBE")

function runlocally(force = false)
	global CURRENT_EXPBACKEND
	if !(typeof(CURRENT_EXPBACKEND) <: LocalExpBackend) || force
		finalize(CURRENT_EXPBACKEND)
		CURRENT_EXPBACKEND = LocalExpBackend()
	end
end

local_results_at(dir, s) = local_results(s; resdir=joinpath(dir, "results_LocalExpBE"))
function local_results(s; resdir = nothing)
	if resdir isa Nothing 
		resdir = joinpath(pwd(), "results_LocalExpBE")
	end
	dir = joinpath(resdir, "results_$s", "logdict")
	t = DataFrame()
	for f in readdir(dir)
		# nf = CSV.read(joinpath(dir,f))
		nf = CSV.read(joinpath(dir,f), DataFrame)
		# @debug "reading CSV" nf unique(nf.fname_C)
		t = vcat_addmissing(t, nf)
	end
	return t
end

isrunninglocally() = CURRENT_EXPBACKEND isa LocalExpBackend

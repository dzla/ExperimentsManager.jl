import Base.==

#######################################################################
#                              Variables                              #
#######################################################################

# Needs to be mutable as we use variables hashes later.
mutable struct Variable{T}
	name::String
	init::T
end
==(u::Variable, v::Variable) = (u.name == v.name) && (u.init == v.init)
Base.hash(v::Variable, h::UInt64) = hash((v.name, v.init), h)

initialize(v::Variable) = deepcopy(v.init)
# Variables cannot be deep-copied (this is because workflows are deep-copied).
Base.deepcopy_internal(v::Variable, stackdict::IdDict) = v
"""
	addvar(e, init; save_output=true)
Creates an experiment variable, which will be initialized at `init` at each run.
"""
function addvar(e, name, init; save_output = true)
	v = Variable(name, init)
	push!(e.prms.vars, v)
	save_output && push!(e.prms.out_vars, v)
	return v
end

# ===== Aggregators =====
aggregate(x::Vector{Vector{T}} where T<:Dict) = isempty(vcat(x...)) ? nothing : reduce(vcat_addmissing, map(d -> DataFrame(d), vcat(x...)))
aggregate(::Vector{Nothing}) = nothing
aggregate(x::Vector{Vector{T}} where T) = vcat(x...)

#######################################################################
#                               Resolve                               #
#######################################################################

function lookup(x, results, prms, vars)
	if x isa WorkflowResult
		results[x.e]
	elseif x isa WorkflowPrm
		prms[x.e][x.prm]
	elseif x isa Variable
		vars[x]
	else
		x
	end
end
function resolve_workflow!(f, results, prms, obs)
	args, kwargs = f.prms.args, f.prms.kwargs
	# Resolve args
	map!(x -> x.first => lookup(x.second, results, prms, obs), args, args)
	# Resolve kwargs
	for k in keys(kwargs)
		kwargs[k] = lookup(kwargs[k], results, prms, obs)
	end
end

#######################################################################
#                          Experiment types                           #
#######################################################################

mutable struct ExpPrms
	name::Union{String, Nothing}
	vars::Array{Variable}
	out_vars::Array{Variable}
	iterations::Int
	logdict::Variable
	resultsdir::Union{String,Nothing}
	datadeps::Array{String}
	modules::Array{Module}
end

mutable struct Exp
	workflow::Array{}
	prms::ExpPrms
end

"""
	Exp(; name = nothing, iterations = 1, workflow = [], datadeps = [])
"""
function Exp(; name = nothing, iterations = 1, workflow = [], datadeps = [])
	d = Variable("logdict", Dict{Symbol,Any}())
	prms = ExpPrms(name, [d], [d], iterations, d, nothing, datadeps, [])
	Exp(workflow, prms)
end
logdict(e::Exp) = e.prms.logdict

struct UnitExp
	prms::ExpPrms
	workflow::Array{}
	informative_name::String
	uniquehash::UInt64
	basename::String
	outpaths::Dict{Variable,String}
end
function UnitExp(e, w)
	# Name using functions' names & hash
	cleaner = (s -> replace(s, "!" => ""))
	funs_names = [string(nameof(we[2].fun))[1:min(8,end)] for we in w]
	uniquehash = hash(w)
	informative_name = cleaner(join(funs_names, "_")[1:min(50, end)])
	# @debug "Created unique hash $uniquehash for $informative_name."
	basename = "$(informative_name)_$(uniquehash)"
	UnitExp(e.prms, w, informative_name, uniquehash, basename, Dict{Variable,String}())
end
unitexps(e) = (UnitExp(e, w) for w in workflow_iterator(e.workflow))


#######################################################################
#                         Running Experiments                         #
#######################################################################

"""
$(SIGNATURES)
Adds the new output `out` to `outputs`.
More precisely, as both `out` and `outputs` are dictionaries with keys corresponding to variable names, each pair `:var => data` in `out` yields `data` being added to `outputs[:var]`.
"""
function update_outputs!(outputs, out::Dict)
	for p in pairs(out)
		push!(outputs[p.first], p.second)
	end
end
update_outputs!(outputs, ::Nothing) = outputs

# ===== Run one Exp (multiple UnitExp with multiple iterations each) =====

modulepath(m) = joinpath(splitpath(pathof(m))[1:end-2]...)

function set_required_modules!(e)
	p = e.prms
	funs = vcat([[p.first for p in we[2]] for we in e.workflow]...)
	p.modules = unique([parentmodule(f) for f in funs])
end

function Base.run(e::Exp, backend_kwargs)
	p = e.prms
	uexp_iterator = unitexps(e)
	set_required_modules!(e)
	# Prepare a dict for output variables that need to be memorized
	outputs = Dict{Variable, Array}(v => Vector{typeof(v.init)}[] for v in p.out_vars)
	for v in p.out_vars sizehint!(outputs[v], length(uexp_iterator)) end
	@assert ~isnothing(p.name) && ~isempty(p.name)
	outputs_folder = "results_$(p.name)"
	p.resultsdir = joinpath(resultsbasedir(), outputs_folder)
	update_backend(e)  # Backend-specific tasks
	# Iterate over parameter space
	for (i, ue) in enumerate(uexp_iterator)
		out = run_on_backend(run, [ue]; backend_kwargs...)
		update_outputs!(outputs, out)
	end
	o = Dict(var => aggregate(outs) for (var, outs) in outputs)
	return length(keys(o)) == 1 ? [o[k] for k in keys(o)][1] : o
end

"""
get_num_threads()
Returns the number of threads used by BLAS.
"""
function get_num_threads()
    blas = LinearAlgebra.BLAS.vendor()
    try # Wrap in a try to catch unsupported blas versions
        if blas == :openblas
            return ccall((:openblas_get_num_threads, Base.libblas_name), Cint, ())
        elseif blas == :openblas64
            return ccall((:openblas_get_num_threads64_, Base.libblas_name), Cint, ())
        elseif blas == :mkl
            return ccall((:MKL_Get_Max_Num_Threads, Base.libblas_name), Cint, ())
        end
        # OSX BLAS looks at an environment variable
        if Sys.isapple()
            return ENV["VECLIB_MAXIMUM_THREADS"]
        end
	catch
    end
    return nothing
end

"""
$(SIGNATURES)
Iterates over the parameter space similarly to `run`, but using several threads or processes.
This function does not return anything, and thus results should be stored to disk.

# Keyword arguments:
- `parallelization_method`: can be any of `threads`, `pfor` or `pmap` (to be preferred over `pfor` as it balances the charge).
- `continue_on_errors`: will log errors and backtrace but not interrupt the experiment.
- `log_to_files`: will log the logging/stdout/stderr to automatically named files (in the logs/ directory of the corresponding experiment). 
- `logging_level`: minimum logging level for log files (e.g. `Logging.Debug`, `Logging.Info`).
- `log_to_console`: will display logging/stdout/stderr on the console. Can be combined with `log_to_files` to get both console and file output.
- `clean_logs`: automatically remove all older log files before running the experiment.
- `BLAS_nthreads`: force using the specified number of threads. If not set, then the number of BLAS threads will be estimated automatically using the number of experiments to run and the number of cores of the machine (but without taking into account the charge caused by other processes!).

See also: [`run`](@run)
"""
function prun(e::Exp, backend_kwargs; 
				continue_on_errors = true, 
				log_to_files = true, 
				log_to_console = false, 
				logging_level = Logging.Info, 
				clean_logs = true, 
				BLAS_nthreads::Int = -1, 
				parallelization_method = :pmap)
	
	p = e.prms
	# TODO some parallel constructions are not compatible with iterators
	uexps = collect(unitexps(e))
	@info "Parameter space of size $(length(uexps))."
	sloop = length(uexps)

	nBLASt = get_num_threads()
	if parallelization_method == :threads
		# Don't add threads here, they must be defined before loading the code.
		(nthreads() < Sys.CPU_THREADS) && @warn "Using only $(nthreads()) threads while $(Sys.CPU_THREADS) logical CPU cores have been detected. You might want to re-launch julia with more threads."
		avail_res = nthreads()
	else
		# Don't create procs here, it must be done manually before loading the code.
		(nprocs() < Sys.CPU_THREADS) && @warn "Using only $(nprocs()) while $(Sys.CPU_THREADS) logical CPU cores have been detected. You might want to re-launch julia with more procs."
		avail_res = nprocs()
	end
	new_nBLASt = if BLAS_nthreads > 0
		if BLAS_nthreads > Sys.CPU_THREADS
			@info "BLAS_nthreads was set to $(BLAS_nthreads), but this core only has $(Sys.CPU_THREADS) CPU threads. Falling back to $(Sys.CPU_THREADS)."
		end
		min(BLAS_nthreads, Sys.CPU_THREADS) # TODO what if we use workers on remote computers?
	elseif sloop < avail_res # Might still use multiple BLAS threads
		Int(round(avail_res/sloop))
	else
		1 # Just parallelize over the loop
	end
	@info "Setting temporarily the BLAS number of threads to $new_nBLASt."
	LinearAlgebra.BLAS.set_num_threads(new_nBLASt)

	set_required_modules!(e)
	@assert ~isnothing(p.name) && ~isempty(p.name)
	outputs_folder = "results_$(p.name)"
	p.resultsdir = joinpath(resultsbasedir(), outputs_folder)
	((log_to_files && clean_logs) && (@warn "Cleaning all existing log files."))
	clean_logs && rm(logs_dir(e); force=true, recursive=true)
	(log_to_files && (@info "Storing logs to $(logs_dir(e))."))
	mkpath(logs_dir(e))
	update_backend(e)  # Backend-specific tasks
	if parallelization_method == :threads
		@threads for ue in uexps
			run_on_backend(run, [ue]; log_to_files, log_to_console, continue_on_errors, logging_level, backend_kwargs...)
		end
	elseif parallelization_method == :pfor
		@sync @distributed for ue in uexps
			run_on_backend(run, [ue]; log_to_files, log_to_console, continue_on_errors, logging_level, backend_kwargs...)
		end
	elseif parallelization_method == :pmap
		@showprogress pmap(uexps) do ue
			run_on_backend(run, [ue]; log_to_files, log_to_console, continue_on_errors, logging_level, backend_kwargs...)
		end
	else
		@error "Unknown parallelization_method '$parallelization_method'."
	end
	@info "Setting back the number of BLAS threads to $nBLASt."
	LinearAlgebra.BLAS.set_num_threads(nBLASt)
	nothing
end

# ===== Run one UnitExp (multiple iterations) =====

function setoutpaths(ue::UnitExp)
	for v in ue.prms.out_vars
		dir = joinpath(ue.prms.resultsdir, "$(v.name)")
		@debug "Creating dir $dir"
		mkpath(dir)
		csvpath = newpath(dir, ue.basename) 	# Create nonexisting file
		ue.outpaths[v] = csvpath
	end
end

# NOTE: this will be ran through the backend
"""
$(SIGNATURES)
Runs an `UnitExp` for `remaining_iterations`, and returns .

See also: [`prun`](@prun)
"""
function Base.run(ue::UnitExp; remaining_iterations = -1)
	setoutpaths(ue) 
	nbits = (remaining_iterations ≥ 0 ? remaining_iterations : ue.prms.iterations)
	# Init outputs
	outputs = Dict{Variable, Any}(v => typeof(v.init)[] for v in ue.prms.out_vars)
	for v in ue.prms.out_vars sizehint!(outputs[v], nbits) end
	for it in 1:nbits
		out = run_one_iteration(ue) # Returns a Dict(:variable1 => data1, …)
		update_outputs!(outputs, out)
		save_outputs(ue, outputs)
	end
	# Reduced outputs: Dict(var => Array of dicts, 1 per it)
	outputs
end

# TODO support other type of exports
# Save variables that should be saved (e.g. to disk)
function save_outputs(ue, outputs)
	processed_outputs = Dict(var => aggregate([outs]) for (var, outs) in outputs)
	for v in ue.prms.out_vars
		csvpath = ue.outpaths[v]
		@info "writing to $csvpath a $(typeof(processed_outputs[v])) of size $(size(processed_outputs[v]))"
		CSV.write(csvpath, processed_outputs[v])
	end
end

# ===== Run one Iteration of UnitExp =====

remove_symbols(a) = [(isa(e, Pair) ? e.second : e) for e in a]
Base.run(f::FunCall) = f.fun(remove_symbols(f.prms.args)...; f.prms.kwargs...)

function initialize_vars(ue::UnitExp)
	vars = Dict{Variable, Any}()
	for v in ue.prms.vars
		vars[v] = initialize(v)
	end
	return vars
end

function run_one_iteration(ue::UnitExp)
	symbs = [we[1] for we in ue.workflow]
	@assert length(unique(symbs)) == length(symbs)	# all different
	results = Dict{Symbol, Any}()
	prms = Dict{Symbol, Dict{Symbol, Any}}()
	vars = initialize_vars(ue)
	for (sb, fc) in ue.workflow
		fcc = deepcopy(fc)
		resolve_workflow!(fcc, results, prms, vars)
		prms[sb] = named_prms(fcc)
		(results[sb], elapsed_time, allocated_bytes, …) = @timed run(fcc)
		# Record statistics
		vars[ue.prms.logdict][Symbol("fname_$(String(sb))")] = nameof(fcc.fun)
		vars[ue.prms.logdict][Symbol("elapsed_time_$(String(sb))")] = elapsed_time
		vars[ue.prms.logdict][Symbol("allocated_bytes_$(String(sb))")] = allocated_bytes
	end
	for key in [:todiscard, :non_numerical]
		(key in keys(vars[ue.prms.logdict])) && delete!(vars[ue.prms.logdict], key)
	end
	Dict(k => vars[k] for k in ue.prms.out_vars)
end

#######################################################################
#                               Helpers                               #
#######################################################################

"""
	vcat_addmissing(tables)
Concatenates vertically all DataFrames in tables, adding `missing` values if necessary.
"""
function vcat_addmissing(tables::Array{DataFrame})
	tables = filter(t -> ~isempty(t), tables)
	for n in unique(vcat(names.(tables)...)), df in tables
		n in names(df) || (df[!,n] .= missing)
	end
	vcat(tables...)
end
vcat_addmissing(df1::DataFrame, df2::DataFrame) = vcat_addmissing([df1, df2])


""" 
	newpath(resultsdir, basename)
Creates file name of the form `basename`_X.csv that does not exists in `resultsdir`.
"""
function newpath(resultsdir, basename)
	path = joinpath(resultsdir, "$(basename)_0.csv")
	while isfile(path)
		sp = rsplit(path[1:end-4], "_"; limit=2)
		path = "$(sp[1])_$(parse(Int,sp[2])+1).csv"
	end
	return path
end

#######################################################################
#                          Parameter spaces                           #
#######################################################################

"""
Parameter space for positional arguments, with indexing symbol 
(for readability only, useless in practice).
"""
const Args = Array{Pair{Symbol,Array}}
"""
Parameter space for keywords arguments.
"""
const KwArgs = Dict{Symbol, Any}

"""
Short representation of a parameter space.
"""
mutable struct PrmSpace
	args::Args
	kwargs::KwArgs
end
PrmSpace(args::Tuple, kwargs) = PrmSpace(collect(args), kwargs)
PrmSpace(a) = PrmSpace(a, Dict())
PrmSpace() = PrmSpace([],Dict())

"""
Representation of a set of function parameters (positional arguments `args` and keyword arguments `kwargs`).
"""
mutable struct PrmSet
	args::Array{Pair{Symbol, Any}}
	kwargs::Dict{Symbol, Any}
end
PrmSet(a) = PrmSet(a, Dict())
PrmSet() = PrmSet([],Dict())
Base.hash(ps::PrmSet, h::UInt64) = hash((ps.args, ps.kwargs), h)

struct FunCall
	fun::Function
	prms::PrmSet
end
# FunCall(s::Symbol, prms) = FunCall(getfield(@__MODULE__, s), prms)
Base.hash(fc::FunCall, h::UInt64) = hash((nameof(fc.fun), fc.prms), h)

named_prms(fc) = merge(Dict(fc.prms.args), fc.prms.kwargs)

#######################################################################
#                              Workflows                              #
#######################################################################

### Workflow

struct WorkflowResult
	e::Symbol		# element of the workflow
end
Base.hash(wr::WorkflowResult, h::UInt64) = hash(wr.e, h)
struct WorkflowPrm
	e::Symbol		# element of the workflow
	prm::Symbol		# name of prm
end
Base.hash(wp::WorkflowPrm, h::UInt64) = hash((wp.e, wp.prm, h))

"""
A workflow with contains only one PrmSet for each function.
"""
struct ReducedWorkflow
	a::Array
end
Base.hash(rw::ReducedWorkflow, h::UInt64) = hash(wp.a)




#######################################################################
#                              Expansion                              #
#######################################################################

### Merge kwargs (there might be nested KwArgs, so more tricky)

"""
expand_value(k) returns a function f such that:
- f(v=>p::KwArgs) returns the union of k=>v and p (expanded)
- f(v) = Dict(k=>v) otherwise
"""
function expand_value(k)
	return function(val)
		if isa(val, Pair)
			if typeof(val.second) <: KwArgs
				exsp = expand_kwargs(val.second)
				return [merge(x, Dict(k=>val.first)) for x in exsp]
			else @error "Wrong type in kwargs, got $(typeof(val.second)), should be KwArg." end
		else
			# Implicit empty parameter space otherwise
			return [Dict(k=>val)]

		end
	end
end

"""
Merge two dictionaries, and throws an error if they have 
a key in common that would be overwritten otherwise.
"""
function merge_secure(d1, d2)
	inter = intersect(keys(d1),keys(d2))
	if ~isempty(inter)
		error("Merging keys with non-empty intersection: ",inter,".")
	end
	return merge(d1,d2)
end

"""
Merge two expanded parameter spaces, i.e. arrays of dicts,
and throws an error if they have at least one key in common.
"""
function merge_expanded_prmspaces(ps1::Array, ps2::Array)
	return [merge_secure(x,y) for x in ps1 for y in ps2]
end

"""
prmSpace = Dict(Sympols=>Arrays) => expanded prm space = [Dict(…) …]
"""
function expand_kwargs(ps)
	arrOfps = [reduce((x,y) -> cat(x, y, dims=1), 
					  expand_value(k).(ps[k]); 
					  init=[])
			   	for k in keys(ps)]
	return reduce(merge_expanded_prmspaces, arrOfps; init=[Dict()])
end


### Expand Args

"""
Example: [:a => [a1,a2], :b => [b1,b2,b3]] will produce:
[[:a=>a1,:b=>b1],[:a=>a1,:b=>b2],[:a=>a1,:b=>b3],
 [:a=>a2,:b=>b1],[:a=>a2,:b=>b2],[:a=>a2,:b=>b3]]
"""
function expand_args(ps::Args)
	if isempty(ps)
		return [[]]
	else
		expTail = expand_args(ps[2:end])
	return [cat(Pair{Symbol,Any}[ps[1].first => a], el, dims=1) 
				for el in expTail for a in ps[1].second]
	end
end

### Expand PrmSpace(s)

"""
Expands a PrmSpace to an array of PrmSet.
"""
function expand_prmspace(ps::PrmSpace)
	expargs = expand_args(ps.args)
	expkwargs = expand_kwargs(ps.kwargs)
	[PrmSet(a, kw) for a in expargs for kw in expkwargs]
end

"""
Expands a pair `:fun=>prmspace` to an array of elements `FunCall(fun, prmsᵢ)`.
"""
expand_pair(p::Pair{T, PrmSpace} where T <: Any) = 
		[FunCall(p.first, prms) for prms in expand_prmspace(p.second)]
expand_pair(p::Pair{T, PrmSet} where T <: Any) = [FunCall(p.first, p.second)]

expand_single(e::Function) = [FunCall(e, PrmSet())]
expand_single(e) = [e]
"""
Returns an array of `FunCall`s, or `[ds]` if `ds` does not refer to a function (e.g. is already an array).
"""
expand_workflow_element_onefunction(e) = isa(e, Pair) ? expand_pair(e) : expand_single(e)
expand_workflow_element_list(l) = vcat([expand_workflow_element_onefunction(e) for e in l]...)

"""
Expands (Symbol, Array{FunCall}) to Array{Tuple{Symbol, FunCall}}
"""
expand_workflow_element(p::Tuple) = [(p[1], e) for e in expand_workflow_element_list(p[2])]

### Expand workflows

function workflow_iterator(w)
	expansions_array = [expand_workflow_element(f) for f in w]
	it = (collect(t) for t in Iterators.product(expansions_array...))
end


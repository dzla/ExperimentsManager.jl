const date_format = "HH:MM:SS"

function logger_with_wid(logger; min_level=Logging.Info)
    logger = MinLevelLogger(logger, min_level)
	# Temporary suppressed for compatibility with GaussianMixtures which accesses min_level property
	# logger = TransformerLogger(logger) do log
	  # merge(log, (; message = "$(Dates.format(Dates.now(), date_format)) ($(myid())) $(log.message)"))
	# end
	return logger
end
